// При загрузке страницы - показать на ней кнопку с текстом "Нарисовать круг".
//  Данная кнопка должна являться единственным контентом в теле HTML документа, 
//  весь остальной контент должен быть создан и добавлен на страницу с помощью Javascript


// При нажатии на кнопку "Нарисовать круг" показывать одно поле ввода - диаметр круга.
//  При нажатии на кнопку "Нарисовать" создать на странице 100 кругов (10*10) случайного цвета. 
// При клике на конкретный круг - этот круг должен исчезать, при этом пустое место заполняться,
//  то есть все остальные круги сдвигаются влево.


const btn1 = document.getElementById('btn');
const arr1 = document.querySelectorAll ("div>div");
const arr2 = [...arr1]; 
const input1 = document.createElement("input");
input1.setAttribute ('placeholder',"Введите диаметр круга");
input1.classList.add ("hidden");
document.body.append(input1);

var color=["red", "blue", "green", "orange", "aqua", "violet" ];



btn1.addEventListener("click", function t(){
   input1.classList.remove("hidden");

   const clientDate = input1.value;
   const cercDiam = parseInt(clientDate);
   const cercRad = cercDiam/2+"px";
   console.log(clientDate);
    if(clientDate !== ""){
       
      for(let i=0; i<arr2.length; i++){

         if (arr2[i].className == "hidden"){
               arr2[i].classList.remove("hidden");
               arr2[i].classList.add("active");
               arr2[i].style.backgroundColor=color[Math.floor(Math.random()*color.length)];
               arr2[i].style.height = cercRad;
               arr2[i].style.width = cercRad;
               arr2[i].addEventListener("click", function(){
                  this.remove();
               })
         }
       }   
    //})
   }
})


