package sample;

public class Hw52 {

	public static void main(String[] args) {
		/* 2) Реализуйте метод параметрами которого являются - целое число, 
 		 * вещественное число и строка. Возвращает он конкатенацию строки 
		 * с суммой вещественного и целого числа.
		 */
		
		int a = 5;
		double b = 7.7;
		String text = "Двенадцать целых, семь десятых";
		String userText = concat( a, b, text);
		System.out.println(userText);

	}


public static String concat(int a, double b, String text){
	String textRes = a+b+" "+text;
	return textRes;
	
}
}
