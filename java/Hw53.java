package sample;

public class Hw53 {

	public static void main(String[] args) {
		/*
		 * 3) Реализуйте метод рисующий на экране прямоугольник из звездочек «*» — его
		 * параметрами будут целые числа которые описывают длину и ширину такого
		 * прямоугольника.
		 */

		int w = 5;
		int h = 10;
		drawRectangle( w, h, '*');

	}

	public static void drawRectangle(int w, int h, char d) {
	for (int i = 0; i < w; i++) {
			for (int j = 0;  j< h; j++) {
				System.out.print(d);
			}
			System.out.println();
	}
	}
}
