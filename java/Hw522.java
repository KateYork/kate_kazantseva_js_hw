package sample;

import java.util.Scanner;

public class Hw522 {

	public static void main(String[] args) {
		// 2) Написать код для возможности создания массива целых чисел
		// (размер вводиться с клавиатуры) и возможности заполнения
		// каждого его элемента вручную. Выведите этот массив на экран.

		Scanner sc = new Scanner(System.in);
		int a;
		System.out.println("Укажите длину массива:");
		a = sc.nextInt();
		int[] arr = new int[a];

		for (int i = 0; i < arr.length; i++) {

			System.out.println("Введите элемент массива:");
			arr[i] = sc.nextInt();

		}

		for (int j = 0; j < arr.length; j++) {
			System.out.print( +arr[j] + " ");
		}
	}
}
