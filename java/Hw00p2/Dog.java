package sample;

public class Dog extends Animal {

	private String name;

	public Dog(String ration, String color, int weight, String name) {
		super(ration, color, weight);
		this.name = name;
	}

	public Dog(String ration, String color, int weight) {
		super(ration, color, weight);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Dog [name=" + name+", "+super.toString() +", "+getVoice()+ "]";
	}

	@Override
	public String getVoice() {
		// TODO Auto-generated method stub
		return "woof woof";
		
	}

	@Override
	public void eat() {
		// TODO Auto-generated method stub
		System.out.println("Meat");
	}

	@Override
	public void sleep() {
		// TODO Auto-generated method stub
		System.out.println("16 hours a day");
	}
	

	

}
