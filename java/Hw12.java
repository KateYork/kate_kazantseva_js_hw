package sample;

import java.util.Scanner;

public class Hw12 {

	public static void main(String[] args) {
		Scanner cerc = new Scanner(System.in);

		System.out.println("Введите радиус круга:");
		int cercRad = cerc.nextInt();
		double cercL = 2 * Math.PI * cercRad;
		System.out.println("Длина окружности составляет: " + cercL);

	}

}
