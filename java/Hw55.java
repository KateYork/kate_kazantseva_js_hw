package sample;

import java.util.Scanner;

public class Hw55 {

	public static void main(String[] args) {
		/* 5) Напишите метод который вернет количество слов в строке текста. */
		Scanner sc = new Scanner(System.in);
		String a;
		System.out.println("Напиши предложение из 5 слов");
		a = new String(sc.nextLine());
		
		int countW = countWords(a)+1;
		System.out.println(countW);

	}

	public static int countWords(String a) {
		int count = 0;
		for (int i = 0; i < a.length(); i++) {
			
			if (a.charAt(i) == ' ') {
				
				 count++;

			}
			
		}
		return count;
	}

}
